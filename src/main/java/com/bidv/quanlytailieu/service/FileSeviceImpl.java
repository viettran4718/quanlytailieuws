package com.bidv.quanlytailieu.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileSeviceImpl implements FileService {
    @Value("${upload.dir}")
    String uploadDir;

    @Override
    public String saveFile(MultipartFile file) throws IOException {
        String dir = uploadDir + "/" + System.currentTimeMillis();
        if (Files.notExists(Paths.get(dir))) {
            try {
                Files.createDirectories(Paths.get(dir));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        String savePath = dir + "/" +file.getOriginalFilename();

        Path filePath = Paths.get(savePath);
        Files.write(filePath, file.getBytes());

        return savePath;
    }
}
