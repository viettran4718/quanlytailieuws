package com.bidv.quanlytailieu.application.service;

import com.bidv.quanlytailieu.domain.entity.DtbUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUserDetails extends User {
    public LoginUserDetails(DtbUser user, Collection<? extends GrantedAuthority> authorities) {
        super(user.getUsername(), user.getPassword(), authorities);
    }
}
