/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package com.bidv.quanlytailieu.application.service;


import com.bidv.quanlytailieu.domain.entity.DtbRole;
import com.bidv.quanlytailieu.domain.entity.DtbUser;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class LoginDetailsService implements UserDetailsService {

    @Autowired
    private DtbUserRepository dtbUserRepository;
//  @Autowired private ConstantProperties constantProperties;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        DtbUser user =
                dtbUserRepository
                        .getDistinctByUsername(username);
        return new LoginUserDetails(user, getAuthorities(user.getRoles()));
    }

    private List<GrantedAuthority> getAuthorities(
            Set<DtbRole> roles) {
        List<GrantedAuthority> authorities
                = new ArrayList<>();
        for (DtbRole role : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
        }
        return authorities;
    }
}
