package com.bidv.quanlytailieu.application.util;

import lombok.Getter;

import javax.validation.constraints.NotNull;

public class DataBuilderUtil {

    private static final String specialSymbol = "!!!@@@";
    @Getter
    private String aesKeyEncrypted;
    @Getter
    private String dataEncrypted;

    public DataBuilderUtil(@NotNull String mixData) {
        String[] split = mixData.split(specialSymbol);
        try {
            this.aesKeyEncrypted = split[0];
            this.dataEncrypted = split[1];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String buildData(String aesKeyEncrypted, String dataEncrypted) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(aesKeyEncrypted);
        stringBuilder.append(specialSymbol);
        stringBuilder.append(dataEncrypted);
        return stringBuilder.toString();
    }

}
