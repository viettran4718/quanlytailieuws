package com.bidv.quanlytailieu.application.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class AesEncryptUtil {
    private static final String ALGO = "AES"; // Default uses ECB PKCS5Padding
    private String secretKey = "DASDA!";

    public AesEncryptUtil() throws NoSuchAlgorithmException {
    //        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
    //        keyGen.init(128); // for example
    //        SecretKey secretKey = keyGen.generateKey();
    ////        this.secretKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
    //
    ////        this.secretKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
    //        this.secretKey = new String(secretKey.toString());

//        this.secretKey = StringUtil.randomString( 16);
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String encryptDefault(String data) throws Exception {
        String encodedBase64Key = encodeKey(getSecretKey());
        Key key = generateKey(encodedBase64Key);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = Base64.getEncoder().encodeToString(encVal);
        return encryptedValue;
    }
    public String encrypt(String data, String secret) throws Exception {
        String encodedBase64Key = encodeKey(secret);
        Key key = generateKey(encodedBase64Key);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = Base64.getEncoder().encodeToString(encVal);
        return encryptedValue;
    }


    private static Key generateKey(String secret) throws Exception {
        byte[] bytesOfMessage = secret.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] b = md.digest(bytesOfMessage);
        Key key = new SecretKeySpec(b , ALGO);

//        byte[] decoded = Base64.getDecoder().decode(secret.getBytes());
//        Key key = new SecretKeySpec(decoded, ALGO);
        return key;
    }

    public static String decodeKey(String str) {
        byte[] decoded = Base64.getDecoder().decode(str.getBytes());
        return new String(decoded);
    }

    private static String encodeKey(String str) {
        byte[] encoded = Base64.getEncoder().encode(str.getBytes());
        return new String(encoded);
    }



}
