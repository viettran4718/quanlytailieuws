package com.bidv.quanlytailieu.application.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SuccessMessage {
    private String message;
}
