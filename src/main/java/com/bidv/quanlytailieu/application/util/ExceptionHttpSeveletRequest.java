package com.bidv.quanlytailieu.application.util;

import com.bidv.quanlytailieu.application.exception.BeforeLoginException;
import com.bidv.quanlytailieu.application.exception.ErrorConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.zalando.problem.Problem;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

public class ExceptionHttpSeveletRequest extends Exception{

    ObjectMapper mapper = new ObjectMapper();

    @AllArgsConstructor
    @Getter
    @Setter
    class ExceptionMessage implements Serializable {
        String title;
        String message;
    }

    public ExceptionHttpSeveletRequest(HttpServletResponse response, BeforeLoginException loginException) {
        super();
        ExceptionMessage exceptionMessage= new ExceptionMessage(loginException.getTitle(), loginException.getDetail());
        response.setStatus(loginException.getStatus().getStatusCode());
        try {
            response.getWriter().write(mapper.writeValueAsString(exceptionMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
