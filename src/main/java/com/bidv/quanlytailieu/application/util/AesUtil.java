package com.bidv.quanlytailieu.application.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class AesUtil {

    private static final String ALGO = "AES"; // Default uses ECB PKCS5Padding
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static String encrypt(String data, String secret) throws Exception {
        String encodedBase64Key = encodeKey(secret);
        Key key = generateKey(encodedBase64Key);
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        String encryptedValue = Base64.getEncoder().encodeToString(encVal);
        return encryptedValue;
    }

    public static String encryptFromObject(Object object, String secret) throws Exception {
        String dataToString = objectMapper.writeValueAsString(object);
        return encrypt(dataToString, secret);
    }

    public static String decrypt(String encrypted, String secret) {
        try {
            String encodedBase64Key = encodeKey(secret);
            Key key = generateKey(encodedBase64Key);
            Cipher cipher = Cipher.getInstance(ALGO);
            cipher.init(Cipher.DECRYPT_MODE, key);
            return new String(cipher.doFinal(Base64.getDecoder().decode(encrypted)));
        } catch (Exception e) {
            System.out.println("Error while decrypting: " + e.toString());
        }
        return null;
    }

    public static Object decryptToObject(String encrypted, String secret, Class neededClass) throws JsonProcessingException {
        String decrypt = decrypt(encrypted, secret);
        Object data = objectMapper.readValue(decrypt, neededClass);
        return data;
    }

    private static Key generateKey(String secret) throws Exception {
        byte[] decoded = Base64.getDecoder().decode(secret.getBytes());
        Key key = new SecretKeySpec(decoded, ALGO);
        return key;
    }

    private static String decodeKey(String str) {
        byte[] decoded = Base64.getDecoder().decode(str.getBytes());
        return new String(decoded);
    }

    private static String encodeKey(String str) {
        byte[] encoded = Base64.getEncoder().encode(str.getBytes());
        return new String(encoded);
    }

    public static String generateSecret() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128); // for example
            SecretKey secretKey = keyGen.generateKey();
            return Base64.getEncoder().encodeToString(secretKey.getEncoded());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }
}
