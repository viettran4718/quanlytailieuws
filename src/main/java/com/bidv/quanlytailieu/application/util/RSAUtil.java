package com.bidv.quanlytailieu.application.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class RSAUtil {

	private static ObjectMapper objectMapper = new ObjectMapper();

	private static PublicKey getPublicKey(String base64PublicKey) {
		PublicKey publicKey = null;
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			publicKey = keyFactory.generatePublic(keySpec);
			return publicKey;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return publicKey;
	}

	private static PrivateKey getPrivateKey(String base64PrivateKey) {
		PrivateKey privateKey = null;
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
		KeyFactory keyFactory = null;
		try {
			keyFactory = KeyFactory.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		try {
			privateKey = keyFactory.generatePrivate(keySpec);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
		return privateKey;
	}

	private static byte[] encrypt(String data, PublicKey publicKey) throws BadPaddingException, IllegalBlockSizeException,
			InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		return cipher.doFinal(data.getBytes());
	}

	public static String encrypt(String data, String publicKeyBase64) throws IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException {
		PublicKey publicKey = getPublicKey(publicKeyBase64);
		byte[] bData = encrypt(data, publicKey);
		return Base64.getEncoder().encodeToString(bData);
	}

	private static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException,
			NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return new String(cipher.doFinal(data));
	}

	public static String decrypt(String data, String base64PrivateKey) throws IllegalBlockSizeException,
			InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
		return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
	}

	public static void main(String[] args) throws IllegalBlockSizeException, InvalidKeyException,
			NoSuchPaddingException, BadPaddingException, NoSuchAlgorithmException, UnsupportedEncodingException {
		KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
//		kpg.initialize(1024);
		KeyPair kp = kpg.generateKeyPair();
		String publicKey = Base64.getEncoder().encodeToString(kp.getPublic().getEncoded());
		String privateKey = Base64.getEncoder().encodeToString(kp.getPrivate().getEncoded());
//		String a = new String(kp.getPublic().getEncoded());
//		System.out.println(a);
		System.out.println("New public key: " + publicKey);
		System.out.println("New private key: " + privateKey);
		try {
			String encryptedString = encrypt("Dhiraj is the author", publicKey);
			System.out.println(encryptedString);
			String decryptedString = RSAUtil.decrypt(encryptedString, privateKey);
			System.out.println(decryptedString);
		} catch (NoSuchAlgorithmException e) {
			System.err.println(e.getMessage());
		}

	}

	/**
	 * @return first element is string of public key, second element is string of private key
	 */
	public String[] generateKeyPair() {
		String[] keyPairStr = new String[2];

		KeyPairGenerator kpg = null;
		try {
//		kpg.initialize(1024);
			kpg = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		KeyPair kp = kpg.generateKeyPair();
		String publicKey = Base64.getEncoder().encodeToString(kp.getPublic().getEncoded());
		String privateKey = Base64.getEncoder().encodeToString(kp.getPrivate().getEncoded());
		keyPairStr[0] = publicKey;
		keyPairStr[1] = privateKey;

		return keyPairStr;
	}
}
