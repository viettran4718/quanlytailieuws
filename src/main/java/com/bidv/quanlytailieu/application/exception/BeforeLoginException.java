package com.bidv.quanlytailieu.application.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.zalando.problem.Status;

@Data
@AllArgsConstructor
public class BeforeLoginException {
    private String title;
    private Status status;
    private String detail;
}
