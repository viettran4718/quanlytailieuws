package com.bidv.quanlytailieu.controller;

import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.application.util.*;
import com.bidv.quanlytailieu.domain.entity.DtbDocument;
import com.bidv.quanlytailieu.domain.repository.DtbDocumentRepository;
import com.bidv.quanlytailieu.domain.vm.DataTransfer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.zalando.problem.Status;

import javax.validation.Valid;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/admin/document")
public class DocumentController {

    @Autowired
    DtbDocumentRepository dtbDocumentRepository;

    ObjectMapper objectMapper = new ObjectMapper();
    String aesSecret = AesUtil.generateSecret();

    @GetMapping
    public ResponseEntity<DataTransfer> getAll(Pageable pageable, @RequestHeader(value = "publicKey") String publicKeyBase64) throws Exception {

        Page<DtbDocument> pageUsers = dtbDocumentRepository.findAll(pageable);

        Map<String, String> otherInfo = PagenationBodyUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageUsers);

        DataTransfer resp = new DataTransfer<>();
        resp.setOtherInformation(otherInfo);
        System.out.println(aesSecret);
//        otherInfo.put("key", RSAUtil.encrypt(aesSecret, publicKeyBase64));
        String aesSecretEncrypted = RSAUtil.encrypt(aesSecret, publicKeyBase64);
        String dataEncrypt = AesUtil.encryptFromObject(pageUsers.getContent(), aesSecret);
//        //Encrypt data
//        String dataString = objectMapper.writeValueAsString(pageUsers.getContent());

//        resp.setData(dataEncrypt);
        System.out.println(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        resp.setData(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        return ResponseEntity.ok().body(resp);
    }

    @PostMapping
    public ResponseEntity<DtbDocument> addUser(@Valid @RequestBody DtbDocument dtbDocument) {
        if (dtbDocument.getId() != null)
            throw new AppLogicException("BAD REQUEST", Status.BAD_REQUEST, "Create id must be null");

        DtbDocument result = dtbDocumentRepository.save(dtbDocument);
        return ResponseEntity.ok(result
        );
    }

    @PutMapping
    public ResponseEntity<DtbDocument> updateUser(@RequestBody DtbDocument dtbDocument) {

        if (dtbDocument.getId() == null)
            throw new AppLogicException("BAD REQUEST", Status.BAD_REQUEST, "Create id must not be null");

        Optional<DtbDocument> found = dtbDocumentRepository.findById(dtbDocument.getId());

        if (!found.isPresent())
            throw new AppLogicException("NOT FOUND", Status.NOT_FOUND, "Document has id " + dtbDocument.getId() + " is not exists");

        DtbDocument result = dtbDocumentRepository.save(dtbDocument);

        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DtbDocument> userDetail(@PathVariable("id") long id) {
        Optional<DtbDocument> userOptional = dtbDocumentRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }
        return ResponseEntity.ok(userOptional.get());
    }

    @DeleteMapping("/{id}")
    public SuccessMessage hardDelete(@PathVariable("id") long id) {
        Optional<DtbDocument> userOptional = dtbDocumentRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("NOT FOUND", Status.NOT_FOUND, "Document not found");
        }
        DtbDocument dtbUser = userOptional.get();
        dtbDocumentRepository.delete(dtbUser);
        return new SuccessMessage("Delete successful");
    }


}
