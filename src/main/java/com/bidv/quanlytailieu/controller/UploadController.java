package com.bidv.quanlytailieu.controller;

import com.bidv.quanlytailieu.service.FileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@RestController
public class UploadController {

    @Autowired
    FileService fileService;

    @PostMapping(value = "/upload")
    @CrossOrigin(origins = "http://localhost:4200")
    public String postFile(@RequestParam(value = "file") MultipartFile file) throws IOException {
        return fileService.saveFile(file);
    }

}
