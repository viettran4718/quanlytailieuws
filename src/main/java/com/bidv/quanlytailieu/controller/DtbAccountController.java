package com.bidv.quanlytailieu.controller;

import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.domain.entity.DtbAccount;
import com.bidv.quanlytailieu.domain.repository.DtbAccountRepository;
import com.bidv.quanlytailieu.domain.vm.DataTransfer;
import com.bidv.quanlytailieu.domain.vm.ObjectCbb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.zalando.problem.Status;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/account")
public class DtbAccountController {

    @Autowired
    DtbAccountRepository dtbAccountRepository;

    @GetMapping("/get-cbb-data")
    public ResponseEntity<DataTransfer> getCbbData(@RequestParam(required = false) String keySearch) throws Exception {
        if (keySearch == null)
            keySearch = "";
        List<Object[]> rawList = dtbAccountRepository.getListCbb(keySearch);
        List<ObjectCbb> lstCbb = new ArrayList<>();
        for (Object[] obj : rawList) {
            ObjectCbb objectCbb = new ObjectCbb();
            objectCbb.setId((Long) obj[0]);
            objectCbb.setText(String.valueOf(obj[1]));
            objectCbb.setExtendValue(String.valueOf(obj[2]));
            lstCbb.add(objectCbb);
        }
        DataTransfer resp = new DataTransfer<>();
        resp.setData(lstCbb);

        return ResponseEntity.ok().body(resp);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DataTransfer<DtbAccount>> userDetail(@PathVariable("id") long id) {
        Optional<DtbAccount> userOptional = dtbAccountRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("Account not found", Status.NOT_FOUND, "Account not found");
        }
        DataTransfer resp = new DataTransfer<>();
        resp.setMessage("Update successfully");
        resp.setData(userOptional.get());

        return ResponseEntity.ok(resp);
    }

}
