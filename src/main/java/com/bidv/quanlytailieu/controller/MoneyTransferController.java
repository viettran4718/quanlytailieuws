package com.bidv.quanlytailieu.controller;

import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.application.util.*;
import com.bidv.quanlytailieu.domain.entity.DtbMoneyTransfer;
import com.bidv.quanlytailieu.domain.repository.DtbMoneyTransferRepository;
import com.bidv.quanlytailieu.domain.vm.DataTransfer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.zalando.problem.Status;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/money-transfer")
public class MoneyTransferController {

    //    private final String aesSecret = AesUtil.generateSecret();
    @Autowired
    DtbMoneyTransferRepository dtbMoneyTransferRepository;
    ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping
    public ResponseEntity<DataTransfer> getAll(Pageable pageable, @RequestHeader(value = "publicKey") String publicKeyBase64) throws Exception {

        Page<DtbMoneyTransfer> pageUsers = dtbMoneyTransferRepository.findAll(pageable);

        Map<String, String> otherInfo = PagenationBodyUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageUsers);

        DataTransfer resp = new DataTransfer<>();
        resp.setOtherInformation(otherInfo);

        String aesSecret = AesUtil.generateSecret();
        System.out.println(aesSecret);
        String aesSecretEncrypted = RSAUtil.encrypt(aesSecret, publicKeyBase64);
        String dataEncrypt = AesUtil.encryptFromObject(pageUsers.getContent(), aesSecret);
        System.out.println(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        resp.setData(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));

        return ResponseEntity.ok().body(resp);
    }

    @PostMapping
    public ResponseEntity<DataTransfer<DtbMoneyTransfer>> addUser(@RequestBody String dataEncryptClient, @RequestHeader(value = "publicKey") String publicKeyBase64) throws Exception {

        DataBuilderUtil dataBuilderUtil = new DataBuilderUtil(dataEncryptClient);
        String aesSecretClient = RSAUtil.decrypt(dataBuilderUtil.getAesKeyEncrypted(), RsaKey.privateKey);
        System.out.println(aesSecretClient);
        DtbMoneyTransfer data = (DtbMoneyTransfer) AesUtil.decryptToObject(dataBuilderUtil.getDataEncrypted(), aesSecretClient, DtbMoneyTransfer.class);

        if (data.getId() != null)
            throw new AppLogicException("BAD REQUEST", Status.BAD_REQUEST, "Create id must be null");

        DataTransfer resp = new DataTransfer<>();
        DtbMoneyTransfer result = dtbMoneyTransferRepository.save(data);
        resp.setMessage("Insert successfully");

        String aesSecret = AesUtil.generateSecret();
        System.out.println(aesSecret);
        String aesSecretEncrypted = RSAUtil.encrypt(aesSecret, publicKeyBase64);
        String dataEncrypt = AesUtil.encryptFromObject(result, aesSecret);
        System.out.println(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        resp.setData(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));

        return ResponseEntity.ok(resp);
    }

    @PutMapping
    public ResponseEntity<DataTransfer<DtbMoneyTransfer>> updateUser(@RequestBody String dataEncryptClient, @RequestHeader(value = "publicKey") String publicKeyBase64) throws Exception {

        DataBuilderUtil dataBuilderUtil = new DataBuilderUtil(dataEncryptClient);
        String aesSecretClient = RSAUtil.decrypt(dataBuilderUtil.getAesKeyEncrypted(), RsaKey.privateKey);
        System.out.println(aesSecretClient);
        DtbMoneyTransfer data = (DtbMoneyTransfer) AesUtil.decryptToObject(dataBuilderUtil.getDataEncrypted(), aesSecretClient, DtbMoneyTransfer.class);

        if (data.getId() == null)
            throw new AppLogicException("BAD REQUEST", Status.BAD_REQUEST, "Update id must not be null");

        Optional<DtbMoneyTransfer> found = dtbMoneyTransferRepository.findById(data.getId());

        if (!found.isPresent())
            throw new AppLogicException("NOT FOUND", Status.NOT_FOUND, "Document has id " + data.getId() + " is not exists");

        DtbMoneyTransfer result = dtbMoneyTransferRepository.save(data);

        DataTransfer resp = new DataTransfer<>();
        resp.setMessage("Update successfully");
        String aesSecret = AesUtil.generateSecret();
        System.out.println(aesSecret);
        String aesSecretEncrypted = RSAUtil.encrypt(aesSecret, publicKeyBase64);
        String dataEncrypt = AesUtil.encryptFromObject(result, aesSecret);
        System.out.println(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        resp.setData(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        return ResponseEntity.ok(resp);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DataTransfer<DtbMoneyTransfer>> userDetail(@PathVariable("id") long id, @RequestHeader(value = "publicKey") String publicKeyBase64) throws Exception {
        Optional<DtbMoneyTransfer> userOptional = dtbMoneyTransferRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }

        DataTransfer resp = new DataTransfer<>();

        String aesSecret = AesUtil.generateSecret();
        System.out.println(aesSecret);
        String aesSecretEncrypted = RSAUtil.encrypt(aesSecret, publicKeyBase64);
        String dataEncrypt = AesUtil.encryptFromObject(userOptional.get(), aesSecret);
        System.out.println(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));
        resp.setData(DataBuilderUtil.buildData(aesSecretEncrypted, dataEncrypt));

        return ResponseEntity.ok(resp);
    }

    @DeleteMapping("/{id}")
    public SuccessMessage hardDelete(@PathVariable("id") long id) {
        Optional<DtbMoneyTransfer> userOptional = dtbMoneyTransferRepository.findById(id);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("NOT FOUND", Status.NOT_FOUND, "Document not found");
        }
        DtbMoneyTransfer dtbUser = userOptional.get();
        dtbMoneyTransferRepository.delete(dtbUser);
        return new SuccessMessage("Delete successful");
    }


}
