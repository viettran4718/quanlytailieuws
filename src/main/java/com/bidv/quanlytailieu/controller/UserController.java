/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package com.bidv.quanlytailieu.controller;


import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.domain.dto.DtbUserDto;
import com.bidv.quanlytailieu.domain.dto.UserLogin;
import com.bidv.quanlytailieu.domain.dto.UserRegister;
import com.bidv.quanlytailieu.domain.entity.DtbRole;
import com.bidv.quanlytailieu.domain.entity.DtbUser;
import com.bidv.quanlytailieu.domain.repository.DtbRoleRepository;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.zalando.problem.Status;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    DtbUserRepository dtbUserRepository;

    @Autowired
    DtbRoleRepository dtbRoleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    ModelMapper modelMapper = new ModelMapper();

    @PostMapping("/register")
    public ResponseEntity<UserLogin> simpleRegisterAccount(
            @Valid @RequestBody UserRegister userRegister) {

        List<DtbRole> roles = dtbRoleRepository.findAll();

        DtbRole role = roles.stream()                        // Convert to stream
                .filter(x -> userRegister.getRole().equals(x.getRoleName()))
                .findAny()                                      // If 'findAny' then return found
                .orElse(null);

        if (role == null) {
            throw new AppLogicException("Role not found", Status.BAD_REQUEST, "Role not found");
        }
        DtbUser found = dtbUserRepository.getDistinctByUsername(userRegister.getUsername());

        if (found != null) {
            if (found.getIsDeleted()) {
                throw new AppLogicException("Cannot create user", Status.BAD_REQUEST, "Please choose another username");
            }

            Set<DtbRole> currentRoles = found.getRoles();

            if (currentRoles.contains(role))
                throw new AppLogicException("Username is exist", Status.BAD_REQUEST, "Username is exist");
            else {

                currentRoles.add(role);
                found.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
                found.setBirthday(userRegister.getBirthday());
                found.setFullname(userRegister.getFullname());
                found.setIsApproved(false);
                found.setIsDeleted(false);
                found.setRoles(currentRoles);
                dtbUserRepository.save(found);
            }
        } else {
            DtbUser user = modelMapper.map(userRegister, DtbUser.class);
            user.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
            user.setIsApproved(false);
            user.setIsDeleted(false);
            Set<DtbRole> currentRoles = new HashSet<>();
            currentRoles.add(role);
            user.setRoles(currentRoles);
            dtbUserRepository.save(user);
        }

        UserLogin userLogin = modelMapper.map(userRegister, UserLogin.class);
        return ResponseEntity.ok(userLogin);
    }

    @GetMapping("/myinfo")
    public ResponseEntity<DtbUser> myInfo(Authentication authentication) {
        String username = authentication.getName();
        DtbUser user = dtbUserRepository.getDistinctByUsername(username);
        return ResponseEntity.ok(user);

    }
}
