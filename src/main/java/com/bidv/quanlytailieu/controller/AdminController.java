package com.bidv.quanlytailieu.controller;

import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.application.util.PaginationUtil;
import com.bidv.quanlytailieu.application.util.RSAUtil;
import com.bidv.quanlytailieu.application.util.RsaKey;
import com.bidv.quanlytailieu.application.util.SuccessMessage;
import com.bidv.quanlytailieu.domain.dto.UserRegister;
import com.bidv.quanlytailieu.domain.entity.DtbRole;
import com.bidv.quanlytailieu.domain.entity.DtbUser;
import com.bidv.quanlytailieu.domain.repository.DtbRoleRepository;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import com.bidv.quanlytailieu.domain.vm.DataEncryptClient;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.zalando.problem.Status;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.validation.Valid;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/admin")
public class AdminController {

    @Autowired
    DtbUserRepository dtbUserRepository;

    @Autowired
    DtbRoleRepository dtbRoleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    ModelMapper modelMapper = new ModelMapper();

    @GetMapping("/users")
    public ResponseEntity<List<DtbUser>> getAll(Pageable pageable) {
        Page<DtbUser> pageUsers = dtbUserRepository.findAll(pageable);

//        Page<DtbUserVm> pageVm = pageUsers.map(new Function<DtbUser, DtbUserVm>() {
//            @Override
//            public DtbUserVm apply(DtbUser user) {
//                DtbUserVm userVm = modelMapper.map(user, DtbUserVm.class);
//                return userVm;
//            }
//        });

        HttpHeaders headers = PaginationUtil
                .generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), pageUsers);
        return ResponseEntity.ok().headers(headers).body(pageUsers.getContent());
    }
//    @PostMapping("/user")
//    public ResponseEntity<DtbUser> addUser(@Valid @RequestBody DataEncryptClient dataEncryptClient) throws InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException {
//
//        String keyDecrypt = RSAUtil.decrypt(dataEncryptClient.getKeyEncrypt(), RsaKey.privateKey);
//        String dataDecrypt = R
//        UserRegister userRegister
//
//        List<DtbRole> roles = dtbRoleRepository.findAll();
//
//        DtbRole role = roles.stream()                        // Convert to stream
//                .filter(x -> userRegister.getRole().equals(x.getRoleName()))
//                .findAny()                                      // If 'findAny' then return found
//                .orElse(null);
//
//        if (role == null) {
//            throw new AppLogicException("Role not found", Status.BAD_REQUEST, "Role not found");
//        }
//        DtbUser found = dtbUserRepository.getDistinctByUsername(userRegister.getUsername());
//        DtbUser result;
//        if (found != null) {
//            if (found.getIsDeleted()) {
//                throw new AppLogicException("Cannot create user", Status.BAD_REQUEST, "Please choose another username");
//            }
//
//            Set<DtbRole> currentRoles = found.getRoles();
//
//            if (currentRoles.contains(role))
//                throw new AppLogicException("Username is exist", Status.BAD_REQUEST, "Username is exist");
//            else {
//
//                currentRoles.add(role);
//                found.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
//                found.setBirthday(userRegister.getBirthday());
//                found.setFullname(userRegister.getFullname());
//                found.setIsApproved(true);
//                found.setIsDeleted(false);
//                found.setRoles(currentRoles);
//                result = dtbUserRepository.save(found);
//            }
//        } else {
//            DtbUser user = modelMapper.map(userRegister, DtbUser.class);
//            user.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
//            user.setIsApproved(true);
//            user.setIsDeleted(false);
//            Set<DtbRole> currentRoles = new HashSet<>();
//            currentRoles.add(role);
//            user.setRoles(currentRoles);
//            result = dtbUserRepository.save(user);
//        }
//
//        return ResponseEntity.ok(result);
//    }
    @PostMapping("/user")
    public ResponseEntity<DtbUser> addUser(@Valid @RequestBody UserRegister userRegister) {
        List<DtbRole> roles = dtbRoleRepository.findAll();

        DtbRole role = roles.stream()                        // Convert to stream
                .filter(x -> userRegister.getRole().equals(x.getRoleName()))
                .findAny()                                      // If 'findAny' then return found
                .orElse(null);

        if (role == null) {
            throw new AppLogicException("Role not found", Status.BAD_REQUEST, "Role not found");
        }
        DtbUser found = dtbUserRepository.getDistinctByUsername(userRegister.getUsername());
        DtbUser result;
        if (found != null) {
            if (found.getIsDeleted()) {
                throw new AppLogicException("Cannot create user", Status.BAD_REQUEST, "Please choose another username");
            }

            Set<DtbRole> currentRoles = found.getRoles();

            if (currentRoles.contains(role))
                throw new AppLogicException("Username is exist", Status.BAD_REQUEST, "Username is exist");
            else {

                currentRoles.add(role);
                found.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
                found.setBirthday(userRegister.getBirthday());
                found.setFullname(userRegister.getFullname());
                found.setIsApproved(true);
                found.setIsDeleted(false);
                found.setRoles(currentRoles);
                result = dtbUserRepository.save(found);
            }
        } else {
            DtbUser user = modelMapper.map(userRegister, DtbUser.class);
            user.setPassword(bCryptPasswordEncoder.encode(userRegister.getPassword()));
            user.setIsApproved(true);
            user.setIsDeleted(false);
            Set<DtbRole> currentRoles = new HashSet<>();
            currentRoles.add(role);
            user.setRoles(currentRoles);
            result = dtbUserRepository.save(user);
        }

        return ResponseEntity.ok(result);
    }

    @PutMapping("/user")
    public ResponseEntity<DtbUser> updateUser(@RequestBody DtbUser dtbUser) {
        Optional<DtbUser> found = dtbUserRepository.findById(dtbUser.getId());

        DtbUser result;

        if (!found.isPresent()) {
            throw new AppLogicException("NOT FOUND", Status.NOT_FOUND, "User has id " + dtbUser.getId() + " is not exist");

        } else {
            dtbUser.setUsername(found.get().getUsername());
            dtbUser.setPassword(found.get().getPassword());
            dtbUser.setRoles(found.get().getRoles());
                result = dtbUserRepository.save(dtbUser);
        }

        return ResponseEntity.ok(result);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<DtbUser> userDetail(@PathVariable("userId") long userId) {
        Optional<DtbUser> userOptional = dtbUserRepository.findById(userId);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }
        return ResponseEntity.ok(userOptional.get());
    }

    @DeleteMapping("/user/{userId}")
    public SuccessMessage hardDelete(@PathVariable("userId") long userId) {
        Optional<DtbUser> userOptional = dtbUserRepository.findById(userId);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }
        DtbUser dtbUser = userOptional.get();
        dtbUser.setRoles(null);
        dtbUserRepository.delete(dtbUser);
        return new SuccessMessage("Delete successful");
    }


    @PostMapping("/delete-user/{userId}")
    public SuccessMessage deleteUser(@PathVariable("userId") long userId) {
        Optional<DtbUser> userOptional = dtbUserRepository.findByIdAndIsDeletedFalse(userId);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }
        DtbUser user = userOptional.get();

        if (user.getIsDeleted())
            throw new AppLogicException("User is deleted", Status.UNAUTHORIZED, "User is deleted");

        user.setIsDeleted(true);
        dtbUserRepository.save(user);
        return new SuccessMessage("Approve successfully");
    }

    @PostMapping("/approve-user/{userId}")
    public SuccessMessage getAll(@PathVariable("userId") long userId) {
        Optional<DtbUser> userOptional = dtbUserRepository.findById(userId);
        if (!userOptional.isPresent()) {
            throw new AppLogicException("User not found", Status.NOT_FOUND, "User not found");
        }
        DtbUser user = userOptional.get();

        if (user.getIsDeleted())
            throw new AppLogicException("User is deleted", Status.BAD_REQUEST, "User is deleted");

        user.setIsApproved(true);
        dtbUserRepository.save(user);
        return new SuccessMessage("Approve successfully");
    }

}
