package com.bidv.quanlytailieu.security;


import com.bidv.quanlytailieu.application.exception.AppLogicException;
import com.bidv.quanlytailieu.application.exception.UnauthorizedException;
import com.bidv.quanlytailieu.domain.entity.DtbRole;
import com.bidv.quanlytailieu.domain.entity.DtbUser;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.bind.annotation.RestController;
import org.zalando.problem.Status;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vietdz on 18/07/2019.
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    DtbUserRepository dtbUserRepository;

    public JWTAuthorizationFilter(AuthenticationManager authManager, DtbUserRepository dtbUserRepository) {
        super(authManager);
        this.dtbUserRepository = dtbUserRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest servletRequest, HttpServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        Authentication authentication = getAuthentication((HttpServletRequest) servletRequest, servletResponse);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, HttpServletResponse servletResponse) {
        String token = request.getHeader(JWTLoginFilter.HEADER_STRING);
        if (token == null)
            throw new UnauthorizedException("User is not login", null);
        Claims claims =
                Jwts.parser()
                        .setSigningKey(JWTLoginFilter.SECRET.getBytes(Charset.forName("UTF-8")))
                        .parseClaimsJws(token.split(" ")[1])
                        .getBody();
        String username = claims.getSubject();
        DtbUser dtbUser = dtbUserRepository.getDistinctByUsername(username);
        if (dtbUser == null) {
            throw new AppLogicException("User not found", Status.UNAUTHORIZED, "Not authorized");
        }
        if (!dtbUser.getIsApproved())
            throw new AppLogicException("User is not approved", Status.UNAUTHORIZED, "Not approved");
        if (dtbUser.getIsDeleted())
            throw new AppLogicException("User is deleted", Status.UNAUTHORIZED, "User is deleted");

        return new UsernamePasswordAuthenticationToken(username, null, getAuthorities(dtbUser.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<DtbRole> roles) {
        List<GrantedAuthority> authorities
                = new ArrayList<>();
        for (DtbRole role : roles) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getRoleName()));
        }

        return authorities;
    }

}
