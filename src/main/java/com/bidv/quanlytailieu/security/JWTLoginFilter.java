package com.bidv.quanlytailieu.security;

import com.bidv.quanlytailieu.application.exception.BeforeLoginException;
import com.bidv.quanlytailieu.application.service.LoginDetailsService;
import com.bidv.quanlytailieu.application.service.LoginUserDetails;
import com.bidv.quanlytailieu.application.util.ExceptionHttpSeveletRequest;
import com.bidv.quanlytailieu.application.util.RsaKey;
import com.bidv.quanlytailieu.domain.dto.UserLogin;
import com.bidv.quanlytailieu.domain.entity.DtbUser;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.zalando.problem.Status;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * Created by vietdz on 18/07/2019.
 */
public class JWTLoginFilter extends UsernamePasswordAuthenticationFilter {

    private DtbUserRepository dtbUserRepository;

    private LoginDetailsService loginDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public static final long EXPIRATIONTIME = 24 * 60 * 60 * 1000; // 1 days
    public static final String SECRET = "dsadhasjdhasjdkasdjkashdjkashkdjashdkajdkslajdklasdjlaksjaksldjaksljljdskljdksldjsalkjalskdjsakldjaslkdjasldkasjdklsajdklsajdlksadjklasjdkasldjasldjaskldjaskldjsalkdjsalkdjsalkdjasdklasjdlkasjdlaskdjaslkdjaskldasjdlkasjdklasdjalskdjaslkdjaslkdjalsdkasjldajsldkahdfyudfuhdsjajdhadhak";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    static final SignatureAlgorithm ALGORITHM = SignatureAlgorithm.HS512;


//	public String generateMD5(String initString) throws NoSuchAlgorithmException {
//		MessageDigest md = MessageDigest.getInstance("MD5");
//		md.update(initString.getBytes());
//		byte byteData[] = md.digest();
//		// convert the byte to hex format method 1
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < byteData.length; i++) {
//			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
//		}
//
//		return sb.toString();
//	}

    public JWTLoginFilter(AuthenticationManager authManager,
                          LoginDetailsService loginDetailsService,
                          BCryptPasswordEncoder bCryptPasswordEncoder,
                          DtbUserRepository dtbUserRepository) {
        //super(new AntPathRequestMatcher(url));
        setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/api/user/login", "POST"));
        //setAuthenticationManager(authManager);
        this.loginDetailsService = loginDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.dtbUserRepository = dtbUserRepository;
    }

    @SneakyThrows
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);
        } catch (Exception e) { /*report an error*/ }

        ObjectMapper objectMapper = new ObjectMapper();
        UserLogin userLogin = new UserLogin();
        try {
            userLogin = objectMapper.readValue(jb.toString(),UserLogin.class);
        } catch (JsonProcessingException e) {
            throw new ExceptionHttpSeveletRequest(response, new BeforeLoginException("Username/password must not empty", Status.BAD_REQUEST, "Username/password must not empty"));
        } catch (Exception e){
            throw new ExceptionHttpSeveletRequest(response, new BeforeLoginException("Other error", Status.BAD_REQUEST, "Other error"));
        }

        DtbUser user = dtbUserRepository.getDistinctByUsername(userLogin.getUsername());

        if (user == null) {
            throw new ExceptionHttpSeveletRequest(response,new BeforeLoginException("User not found", Status.UNAUTHORIZED, "Not authorized"));
        }
        if (!user.getIsApproved())
            throw new ExceptionHttpSeveletRequest(response,new BeforeLoginException("User is not approved", Status.UNAUTHORIZED, "Not approved"));
        if (user.getIsDeleted())
            throw new ExceptionHttpSeveletRequest(response,new BeforeLoginException("User is deleted", Status.UNAUTHORIZED, "User is deleted"));

        LoginUserDetails userDetails = (LoginUserDetails) loginDetailsService.loadUserByUsername(userLogin.getUsername());
        if (userLogin.getPassword() == null || userLogin.getPassword().trim().equals(""))
            throw new ExceptionHttpSeveletRequest(response, new BeforeLoginException("Username/password must not empty", Status.BAD_REQUEST, "Username/password must not empty"));
        if (bCryptPasswordEncoder.matches(userLogin.getPassword(), userDetails.getPassword())) {
            return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), userDetails.getAuthorities());
        } else {
            throw new ExceptionHttpSeveletRequest(response, new BeforeLoginException("Wrong username/password", Status.FORBIDDEN, "Wrong username/password"));
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
//        TokenAuthenticationService.addAuthentication(response, authResult.getName());
        String token = generateToken(authResult);
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
        response.addHeader("publicKey", RsaKey.publicKey);
        response.setContentType("application/json");
        response.getWriter().write("{\"token\" : \"" + token + "\"\n, \"publicKey\" : \"" + RsaKey.publicKey + "\" }");
    }

    String generateToken(Authentication authentication) {
        return Jwts.builder().setSubject(authentication.getName())
                .claim("AUTH", authentication.getAuthorities())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(ALGORITHM, SECRET.getBytes(Charset.forName("UTF-8")))
                .compact()
                ;
    }

}
