package com.bidv.quanlytailieu;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@SpringBootApplication
public class QuanlytailieuApplication extends SpringBootServletInitializer {

    @Value("${upload.dir}")
    String uploadDir;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        if (Files.notExists(Paths.get(uploadDir))) {
            try {
                Files.createDirectories(Paths.get(uploadDir));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return application.sources(QuanlytailieuApplication.class);
    }

//    @Bean
//    public StringHttpMessageConverter stringHttpMessageConverter() {
//        return new StringHttpMessageConverter(Charset.forName("UTF-8"));
//    }

    public static void main(String[] args) {
        SpringApplication.run(QuanlytailieuApplication.class, args);
    }

}
