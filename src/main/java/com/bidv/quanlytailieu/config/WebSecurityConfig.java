/**
 * Copyright (c) 2018, gmenu Inc.<br>
 * All rights reserved.
 */
package com.bidv.quanlytailieu.config;

import com.bidv.quanlytailieu.application.service.LoginDetailsService;
import com.bidv.quanlytailieu.domain.enums.RoleEnum;
import com.bidv.quanlytailieu.domain.repository.DtbUserRepository;
import com.bidv.quanlytailieu.security.JWTAuthorizationFilter;
import com.bidv.quanlytailieu.security.JWTLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

// import com.tmg.cargolink.security.JWTAuthenticationFilter;

/**
 * Created by vietdz on 18/07/2019.
 */
@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends BaseSecurityConfig {

    @Autowired
    private LoginDetailsService loginDetailsService;
    @Autowired
    DtbUserRepository dtbUserRepository;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//    http.cors()
//        .configurationSource(this.corsConfigurationSource())
//        .and()
//        .antMatcher("/api/**")
//        .authorizeRequests()
//        .antMatchers("/api/login")
//        .permitAll()
//        .antMatchers("/api/**")
//        .authenticated()
//        .and()
//        .logout()
//        .and()
//        .csrf()
//        .disable()
//        .addFilter(
//            new JWTLoginFilter(
//                authenticationManager(), loginMobileDetailsService, bCryptPasswordEncoder()))
//        .addFilter(new JWTAuthorizationFilter(authenticationManager()))
//        .sessionManagement()
//        .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.cors()
                .configurationSource(this.corsConfigurationSource())
                .and()
                .antMatcher("/api/**")
                .authorizeRequests()
                .antMatchers("/api/user/login").permitAll()
                .antMatchers("/api/" + RoleEnum.USER.getText() + "/**").hasAnyRole(RoleEnum.USER.getText(),RoleEnum.ADMIN.getText())
                .antMatchers("/api/" + RoleEnum.ADMIN.getText() + "/**").hasRole(RoleEnum.ADMIN.getText())
                .antMatchers("/api/**")
                .authenticated()
                .and()
                .logout()
                .and()
                .csrf()
                .disable()
                .addFilter(
                        new JWTLoginFilter(
                                authenticationManager(), loginDetailsService, bCryptPasswordEncoder(),dtbUserRepository))
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), dtbUserRepository))
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/api/user/register");
        web.ignoring().antMatchers("api/" + RoleEnum.ANONYMOUS.getText() + "/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    /*auth.inMemoryAuthentication().withUser("admin").password("password").roles("ADMIN");
    auth.jdbcAuthentication().dataSource(dataSource)
    		.usersByUsernameQuery("select username, password, 'true' from dtb_user where username=?")
    		.authoritiesByUsernameQuery("select username, 'true' from dtb_user where username=?");*/
        auth.userDetailsService(loginDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
