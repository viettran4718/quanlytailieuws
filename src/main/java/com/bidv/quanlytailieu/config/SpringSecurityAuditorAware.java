package com.bidv.quanlytailieu.config;

import com.bidv.quanlytailieu.domain.enums.RoleEnum;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            return Optional.of(authentication.getName());
        }catch(Exception e) {
            return Optional.of(RoleEnum.ANONYMOUS.getText());
        }

    }
}
