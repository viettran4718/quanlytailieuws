package com.bidv.quanlytailieu.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtbUserDto implements Serializable {

    Long id;

    private String username;

    private String fullname;

    private Boolean isApproved;

    private Boolean isDeleted;

    private Timestamp birthday;

}
