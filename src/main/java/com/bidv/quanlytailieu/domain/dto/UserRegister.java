package com.bidv.quanlytailieu.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class UserRegister implements Serializable {

    @NotNull
    @Size(min = 6, max = 100)
    private String username;

    @NotNull
    @Size(min = 6, max = 100)
    private String fullname;

    @Size(min = 6, max = 100)
    @NotNull
    private String password;

    @Pattern(regexp = "^(84)((3|5|7|8|9){1})(\\d{8})$", message = "Username must be phone number of Vietnam")
    private String phoneNumber;

    @Pattern(regexp = "^[a-z][a-z0-9_\\.]{5,32}@[a-z0-9]{2,}(\\.[a-z0-9]{2,4}){1,2}$")
    private String email;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp birthday;

    private String role;

}
