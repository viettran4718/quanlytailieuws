package com.bidv.quanlytailieu.domain.vm;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DtbUserVm implements Serializable {
    Long id;
    private String username;
    private String fullname;
    private String password;
    private String phoneNumber;
    private String email;
    private Boolean isApproved;
    private Boolean isDeleted;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Timestamp birthday;
}
