package com.bidv.quanlytailieu.domain.vm;

import lombok.*;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTransfer<T> {

    int status = 200;
    boolean isError = false;
    String message = "";
    T data;
    Map<String, String> otherInformation;
}
