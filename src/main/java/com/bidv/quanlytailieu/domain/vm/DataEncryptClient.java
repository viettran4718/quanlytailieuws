package com.bidv.quanlytailieu.domain.vm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataEncryptClient {
    String data;
    String keyEncrypt;
}
