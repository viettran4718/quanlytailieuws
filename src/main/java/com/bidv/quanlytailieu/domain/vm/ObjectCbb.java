package com.bidv.quanlytailieu.domain.vm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ObjectCbb {
    public Long id;
    public String value;
    public String text;
    public String extendValue;
    public String description;

}
