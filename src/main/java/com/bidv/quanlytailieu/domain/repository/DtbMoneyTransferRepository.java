package com.bidv.quanlytailieu.domain.repository;

import com.bidv.quanlytailieu.domain.entity.DtbMoneyTransfer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DtbMoneyTransferRepository extends JpaRepository<DtbMoneyTransfer, Long> {
}
