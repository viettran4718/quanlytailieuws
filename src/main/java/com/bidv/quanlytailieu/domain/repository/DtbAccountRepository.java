package com.bidv.quanlytailieu.domain.repository;

import com.bidv.quanlytailieu.domain.entity.DtbAccount;
import com.bidv.quanlytailieu.domain.vm.ObjectCbb;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DtbAccountRepository extends JpaRepository<DtbAccount, Long> {

    @Query("select t.id, t.accountNumber, a.fullname from DtbAccount t join t.accountOwner a where t.accountNumber like %:keySearch% or a.fullname like %:keySearch%")
    List<Object[]> getListCbb(@Param( value = "keySearch") String keySearch);
}
