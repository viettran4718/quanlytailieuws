package com.bidv.quanlytailieu.domain.repository;

import com.bidv.quanlytailieu.domain.entity.DtbDocument;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DtbDocumentRepository extends JpaRepository<DtbDocument, Long> {
}
