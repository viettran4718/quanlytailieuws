package com.bidv.quanlytailieu.domain.repository;

import com.bidv.quanlytailieu.domain.entity.DtbRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DtbRoleRepository extends JpaRepository<DtbRole, Long> {
}
