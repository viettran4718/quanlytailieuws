package com.bidv.quanlytailieu.domain.repository;

import com.bidv.quanlytailieu.domain.entity.DtbUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DtbUserRepository extends JpaRepository<DtbUser, Long> {
    DtbUser getDistinctByUsername(String username);

    Page<DtbUser> findAllByIsDeletedFalse(Pageable pageable);

    Optional<DtbUser> findByIdAndIsDeletedFalse(Long id);

}
