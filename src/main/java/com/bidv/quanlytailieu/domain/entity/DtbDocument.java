package com.bidv.quanlytailieu.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "dtb_document")
public class DtbDocument extends BaseEnity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Size(max = 100)
    @Column(name = "title", length = 100, nullable = false)
    @NotNull
    String title;

    @Size(max = 5000)
    @Column(name = "content", length = 5000, nullable = false)
    String content;

    @Size(max = 300)
    @Column(name = "file", length = 300)
    String file;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "publish_date")
    Timestamp publishDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "start_date")
    Timestamp startDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "end_date")
    Timestamp endDate;


}
