package com.bidv.quanlytailieu.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "dtb_money_transfer")
public class DtbMoneyTransfer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    //    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trade_code")
//    @SequenceGenerator(name="trade_code", sequenceName = "trade_code_seq", initialValue = 100000000, allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "trade_code", nullable = false, unique = true)
//    Long tradeCode;

    @Size(max = 500)
    @Column(name = "note", length = 500)
    String note;

    @Column(name = "amount", nullable = false)
    Long amount;

    @Column(name = "status", nullable = false)
    Long status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender")
    DtbAccount sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "receiver")
    DtbAccount receiver;


}
