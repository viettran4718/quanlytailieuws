package com.bidv.quanlytailieu.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "dtb_account")
public class DtbAccount extends BaseEnity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Size(min = 5, max = 15, message = "size 5 - 15")
    @Pattern(regexp="^[a-zA-Z0-9]+",message="account number must be digit")
    @Column(name = "account_number", length = 20, unique = true, nullable = false)
    String accountNumber;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "expired_date")
    Timestamp expiredDate;

    @Column(name = "account_balance")
    Long accountBalance;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "account_owner")
    DtbUser accountOwner;

    @OneToMany(mappedBy = "sender", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<DtbMoneyTransfer> senders;

    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<DtbMoneyTransfer> receivers;

}
