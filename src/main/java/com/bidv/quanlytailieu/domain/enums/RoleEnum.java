package com.bidv.quanlytailieu.domain.enums;

public enum RoleEnum {
    ADMIN(1,"admin"),
    USER(2, "user"),
    ANONYMOUS(3,"anonymous");

    int code;
    String text;

    private RoleEnum(int code, String text) {
        this.code = code;
        this.text = text;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
